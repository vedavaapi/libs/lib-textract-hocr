import json
import sys

from requests import HTTPError
from . import svg_helper

from .hocr_parser import HOCRDocument


def fragment_selector_from_hocr_elem(hocr_elem):
    rectangle = {}
    xywh = hocr_elem.xywh
    rectangle.update({
        "x": int(xywh[0]),
        "y": int(xywh[1]),
        "w": int(xywh[2]),
        "h": int(xywh[3])
    })

    fragment_selector = {
        "jsonClass": "FragmentSelector",
        "value": "xywh={},{},{},{}".format(rectangle['x'], rectangle['y'], rectangle['w'], rectangle['h']),
        "rectangle": rectangle
    }
    return fragment_selector


def svg_selector_from_hocr_elem(hocr_elem):
    svg_selector = {
        "jsonClass": "SvgSelector",
        "value": svg_helper.svg_for_polygon(hocr_elem.vertices)
    }
    return svg_selector


def get_section_res(source, index, hocr_elem, section_type):
    selector = {
        "jsonClass": "SvgFragmentSelectorChoice",
        "default": fragment_selector_from_hocr_elem(hocr_elem),
        "item": svg_selector_from_hocr_elem(hocr_elem)
    }

    section = {
        "jsonClass": "Resource",
        "source": source,
        "selector": selector,
        "state": "system_inferred",
        "jsonClassLabel": section_type,
    }

    if index:
        section['index'] = index

    return section


def get_members_selector(resource_ids=None):
    members_selector = {
        # "jsonClass": "MembersSelector"
    }
    if isinstance(resource_ids, list):
        members_selector['resource_ids'] = resource_ids
    return members_selector


def marshal_hocr_element_tree(page_id, source, hocr_elem, index, depth, hierarchy, regions_graph, are_blank_ids=True, is_last_elem=False):

    section_types = ('HOCRPage', 'HOCRCArea', 'HOCRParagraph', 'HOCRLine', 'HOCRWord')

    if depth < 4:
        node = get_section_res(source, index, hocr_elem, section_types[depth])
        node_id = ('_:' if are_blank_ids else '') + hocr_elem.id
        node['_id'] = node_id
        child_hocr_elem_ids = []

        for i, child in enumerate(hocr_elem.children):
            child_node_id = marshal_hocr_element_tree(
                page_id, node_id,
                child, i, depth + 1, hierarchy, regions_graph,
                are_blank_ids=are_blank_ids, is_last_elem=(i == len(hocr_elem.children) - 1)
            )
            child_hocr_elem_ids.append(child_node_id)
        if depth == 3:
            node['members'] = get_members_selector(resource_ids=child_hocr_elem_ids)

        hierarchy[node_id] = node

    else:
        node = get_section_res(page_id, None, hocr_elem, section_types[depth])
        node_id = ('_:' if are_blank_ids else '') + hocr_elem.id
        node['_id'] = node_id
        node['jsonClass'] = 'ImageRegion'

        text = hocr_elem.ocr_text
        text_anno_id = hocr_elem._hocr_elem.get('data_anno_id', '_:{}-anno'.format(hocr_elem.id))
        anno = {
            "_id": text_anno_id,
            "jsonClass": "TextAnnotation",
            "jsonClassLabel": "HOCRWordText",
            "target": node_id,
            "body": [{"jsonClass": "Text", "chars": text + (' ' if not is_last_elem else '\n')}],
            "state": "system_inferred"
        }
        regions_graph.update({
            node_id: node,
            text_anno_id: anno
        })

    return node_id


def set_resolved_region_ids(graph, region_id_uid_graph):
    for item in graph.values():
        if not item.get('members', {}).get('resource_ids', []):
            continue
        resource_ids = item['members']['resource_ids']
        for i, rid in enumerate(resource_ids):
            resource_ids[i] = region_id_uid_graph[rid]


def import_hocr(vc, page_id, hocr):
    hocr_doc = HOCRDocument(hocr)
    if not hocr_doc or not hocr_doc.npages:
        return None, 404
    hocr_page = hocr_doc.pages[0]

    hierarchy = {}
    regions_graph = {}
    marshal_hocr_element_tree(page_id, page_id, hocr_page, 0, 0, hierarchy, regions_graph, are_blank_ids=True)

    graph_post_url = 'objstore/v1/graph'
    regions_graph_post_data = {
        "graph": json.dumps(regions_graph),
        "should_return_resources": "false",
        "should_return_oold_resources": "false"
    }

    try:
        print("posting regions_graph", file=sys.stderr)
        regions_post_response = vc.post(graph_post_url, data=regions_graph_post_data)
        regions_post_response.raise_for_status()
    except HTTPError as e:
        print("error: ", e.response.json())
        raise e

    set_resolved_region_ids(hierarchy, regions_post_response.json()['graph'])

    hierarchy_graph_post_data = {
        "graph": json.dumps(hierarchy),
        "should_return_resources": "false",
        "should_return_oold_resources": "false"
    }

    try:
        print("posting hierarchy", file=sys.stderr)
        hierarchy_post_response = vc.post(graph_post_url, data=hierarchy_graph_post_data)
        hierarchy_post_response.raise_for_status()
    except HTTPError as e:
        print("error: ", e.response.json())
        raise e

    return regions_post_response.json(), hierarchy_post_response.json()


def update_hocr(vc, hocr):

    modded_anno_elems = hocr.findall(".//span[@class='ocrx_word'][@data-modified='true']")
    print(len(modded_anno_elems))
    modded_annos = []
    for elem in modded_anno_elems:
        if not elem.get('data-anno-id', None):
            continue
        prev_state = elem.get('data-anno-state', 'user_supplied')
        new_state = 'user_supplied' if prev_state == 'user_supplied' else 'user_confirmed'
        anno = {
            "jsonClass": "TextAnnotation",
            "_id": elem.get('data-anno-id'),
            "state": new_state,
            "body": [
                {
                    "jsonClass": "Text",
                    "chars": elem.text
                }
            ]
        }
        modded_annos.append(anno)
    post_data = {
        "resource_jsons": json.dumps(modded_annos)
    }
    try:
        response = vc.post('objstore/v1/resources', data=post_data)
        response.raise_for_status()
    except HTTPError as e:
        print('error is: ', e.response.json())
        raise e

    return response.json()


'''
from vedavaapi.client import VedavaapiSession
vs = VedavaapiSession('https://apps.vedavaapi.org/prod/api')
# vs.signin('info@vedavaapi.org', '1234')
vs.access_token = 'QcUUD35NuuE22MNjwuS9D0KPR60yfEydPjjGKWqSti'
from vedavaapi.client.importers import tesseract_importer as ti
# path = '/home/damodarreddy/పొందినవి/default.jpg'
# ti.create_segment_resources(vs, '5cdb06832df5b0000acf6ae2', 'Devanagari', selector_choice=True)
ti.create_segment_resources(vs, '5ce3df9c2df5b0000acf87a9', 'Devanagari')


from vedavaapi.client.importers import tesseract_importer as ti
from vedavaapi.client.importers.hocr_parser import Page, HOCRDocument
hocr_path = '/home/damodarreddy/పొందినవి/గురుకులం/vedavaapi/client-scripts/vedavaapi/client/importers/test.hocr.html'
hocr_doc = HOCRDocument(hocr_path, is_path=True)
hocr_page = hocr_doc.pages[0]

hierarchy = {}
regions_graph = {}
ti.import_hocr_element('page_id', 'page_id', hocr_page, 0, 0, hierarchy, regions_graph, are_blank_ids=True)

'''
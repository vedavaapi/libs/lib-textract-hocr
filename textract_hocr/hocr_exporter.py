import json
import os, sys
from http.client import HTTPException

from requests import HTTPError
from vedavaapi.client import objstore
from lxml import etree


hocr_elem_conf_map = {
    "HOCRPage": ('div', 'ocr_page'),
    "HOCRCArea": ('div', 'ocr_carea'),
    "HOCRParagraph": ('p', 'ocr_par'),
    "HOCRLine": ('span', 'ocr_line'),
    "HOCRWord": ('span', 'ocrx_word')
}


def get_tree(graph, root_node_id, traversed_graph_ids: set, sort_by_index=True):
    tree = {}
    root_node = graph[root_node_id]

    tree['content'] = root_node
    traversed_graph_ids.append(root_node_id)  # NOTE depth first loop resolve.
    tree['specific_resources'] = []
    tree['annotations'] = []

    reached_ids = root_node.get('_reached_ids')
    if not reached_ids:
        return tree

    links_conf = {
        "source": {"group": "specific_resources", "linked_ids": reached_ids.get("source", None)},
        "target": {"group": "annotations", "linked_ids": reached_ids.get("target", None)},
    }
    for link, conf in links_conf.items():
        linked_ids = conf['linked_ids']
        if not linked_ids:
            continue
        tree[conf['group']] = [get_tree(graph, lid, traversed_graph_ids, sort_by_index=sort_by_index) for lid in linked_ids if lid not in traversed_graph_ids]
        if sort_by_index:
            tree[conf['group']] = sorted(tree[conf['group']], key=lambda t: t['content'].get('index', 0))

    return tree

def _append_hocr_element(parent_elem, vv_tree, image_url):
    content = vv_tree['content']
    rectangle = content.get('selector', {}).get('default', {}).get('rectangle', None)
    print({"rectangle": rectangle}, file=sys.stderr)
    if not rectangle:
        return
    hocr_elem_conf = hocr_elem_conf_map.get(content.get('jsonClassLabel', None), None)
    print({"hec": hocr_elem_conf}, file=sys.stderr)
    if not hocr_elem_conf:
        return

    title = 'bbox {} {} {} {}; '.format(
        rectangle['x'], rectangle['y'],
        rectangle['x'] + rectangle['w'], rectangle['y'] + rectangle['h'])
    print(title, file=sys.stderr)

    if content['jsonClassLabel'] == 'HOCRPage':
        title = 'image "{}"; '.format(image_url) + title

    elem = etree.SubElement(parent_elem, hocr_elem_conf[0], title=title, id=content['_id'])
    elem.set('class', hocr_elem_conf[1])
    elem.set('data-state', content.get('state', 'user_created'))

    if content['jsonClassLabel'] == 'HOCRLine':
        return

    spr_branches = vv_tree.get('specific_resources', [])
    for sprb in spr_branches:
        _append_hocr_element(elem, sprb, image_url)
    return


def attach_hocr_words(hocr_tree, regions_tree, hierarchy):
    print('in attach hocr words"')
    id_tree_map = dict((x['content']['_id'], x) for x in regions_tree['specific_resources'])
    line_elems = hocr_tree.findall(".//span[@class='ocr_line']")

    def _append_word_elem(_res_tree, _parent):
        region = _res_tree['content']
        rectangle = region.get('selector', {}).get('default', {}).get('rectangle', None)
        if not rectangle:
            return
        annos = _res_tree.get('annotations', [])
        if not annos:
            print('no annos', file=sys.stderr)
            return
        anno = annos[0]['content']
        title = 'bbox {} {} {} {}; '.format(
            rectangle['x'], rectangle['y'],
            rectangle['x'] + rectangle['w'], rectangle['y'] + rectangle['h']
        )
        word_elem = etree.SubElement(_parent, 'span', title=title, id=region['_id'])
        word_elem.set('class', 'ocrx_word')
        word_elem.set('data-state', region.get('state', 'user_created'))
        word_elem.set('data-anno-id', anno['_id'])
        word_elem.set('data-anno-state', anno.get('state', 'system_inferred'))
        word_elem.text = anno.get('body', [{}])[0].get('chars', '')
        word_elem.tail = ' '

    print('les', len(line_elems), file=sys.stderr)
    if len(line_elems):
        for line in line_elems:
            _id = line.get('id', None)
            vv_res = hierarchy[_id]
            resource_ids = vv_res.get('members', {}).get('resource_ids', [])
            print(_id, resource_ids, file=sys.stderr)
            for res_id in resource_ids:
                res_tree = id_tree_map.get(res_id, None)
                if not res_tree:
                    print('no matching resource tree for ', res_id, file=sys.stderr)
                    continue
                _append_word_elem(res_tree, line)
    else:
        page_elem = hocr_tree.find(".//div[@class='ocr_page']")
        if page_elem is None:
            print("page elem is none")
            return
        for _reg_tree in regions_tree['specific_resources']:
            _append_word_elem(_reg_tree, page_elem)


def to_hocr(vc, page_id):
    root_elem = etree.Element('html')
    head_html = '''
         <head>
            <title></title>
            <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
            <meta name='ocr-system' content='tesseract 3.05.00dev' />
            <meta name='ocr-capabilities' content='ocr_page ocr_carea ocr_par ocr_line ocrx_word ocrp_lang ocrp_dir ocrp_font ocrp_fsize ocrp_wconf'/>
        </head>
        '''
    head_elem = etree.fromstring(head_html)
    root_elem.append(head_elem)

    body_elem = etree.SubElement(root_elem, 'body')

    page_graph_response = objstore.get_graph(
        vc, {"_id": page_id, "jsonClass": "ScannedPage"},
        [
            {"source": {"jsonClass": "Resource"}},
            {"source": {"jsonClass": "Resource"}}
        ],
        direction='referrer', max_hops=4,
        include_incomplete_paths=True, json_class_projection_map = {"*": {"resolvedPermissions": 0}})

    graph = page_graph_response['graph']
    # print(graph, file=sys.stderr)
    start_nodes_ids = page_graph_response['start_nodes_ids']
    if not len(start_nodes_ids):
        raise HTTPError('page not found', 404)

    page_id = start_nodes_ids[0]
    tree = get_tree(graph, page_id, [])
    # print(tree)
    page = tree['content']
    image_oold = page.get('representations', {}).get('stillImage', [])[0].get('data', None)
    page_repr_url = '' if not image_oold else os.path.join(vc.base_url, 'objstore/v1/files/{}'.format(image_oold[6:]))

    sprs = tree['specific_resources']
    if len(sprs):
        hocr_tree = sprs[0]
        _append_hocr_element(body_elem, hocr_tree, page_repr_url)
    else:
        image_info = vc.get('iiif_image/v1/objstore/{}/info.json'.format(image_oold[6:])).json()
        print(image_info)
        bbox = [str(c) for c in (0, 0, image_info['width'], image_info['height'])]
        implicit_page_title = 'image "{}"; bbox "{}"'.format(page_repr_url, ' '.join(bbox))
        implicit_page_elem = etree.SubElement(body_elem, 'div', title=implicit_page_title, id=page_id)
        implicit_page_elem.set('class', 'ocr_page')
        implicit_page_elem.set('data-implicit', 'true')

    region_graph_response = objstore.get_graph(
        vc, {"jsonClass": "ScannedPage", "_id": page_id},
        [{"source": {"jsonClass": "ImageRegion"}}, {"target": {"jsonClass": "TextAnnotation"}}],
        direction='referrer', json_class_projection_map={"*": {"resolvedPermissions": 0}}, max_hops=2
    )
    if not len(region_graph_response['start_nodes_ids']):
        raise HTTPError('no annotations found', 404)

    regions_graph = region_graph_response['graph']
    # print(regions_graph, file=sys.stderr)
    regions_tree = get_tree(regions_graph, page_id, [])
    # print(regions_tree)
    attach_hocr_words(body_elem, regions_tree, graph)

    return root_elem


def persist_changes(vc, hocr):
    modded_anno_elems = hocr.findall(".//span[@class='ocrx_word'][@data-modified='true']")
    print(len(modded_anno_elems))
    modded_annos = []
    for elem in modded_anno_elems:
        if not elem.get('data-anno-id', None):
            continue
        prev_state = elem.get('data-anno-state', 'user_supplied')
        new_state = 'user_supplied' if prev_state == 'user_supplied' else 'user_confirmed'
        anno = {
            "jsonClass": "TextAnnotation",
            "_id": elem.get('data-anno-id'),
            "state": new_state,
            "body": [
                {
                    "jsonClass": "Text",
                    "chars": elem.text
                }
            ]
        }
        modded_annos.append(anno)
    post_data = {
        "resource_jsons": json.dumps(modded_annos),
        "return_projection": json.dumps({"_id": 1, "jsonClass": 1})
    }
    print(post_data)
    anno_post_response = vc.post('objstore/v1/resources', data=post_data)

    '''
    tb_deleted_elems = hocr.findall(".//*[@data-deleted='true']")
    tb_deleted_ids = []
    for elem in tb_deleted_elems:
        
        tb_deleted_ids.append(elem.get('id'))
        if elem.get('data-anno-id', None):
            tb_deleted_ids
    '''
    # print(response.json())
    return anno_post_response



'''
from vedavaapi.client import VedavaapiSession
from lxml import etree
vs = VedavaapiSession('https://apps.vedavaapi.org/prod/api')
# vs.signin('info@vedavaapi.org', '1234')
vs.access_token = 'QcUUD35NuuE22MNjwuS9D0KPR60yfEydPjjGKWqSti'
from vedavaapi.client.importers import hocr_exporter as he
hocr = he.to_hocr(vs, '5ce3df9c2df5b0000acf87a9')

'''

from abc import ABCMeta, abstractmethod
from lxml import etree
import re


class HOCRElement:

    __metaclass__ = ABCMeta

    COORDINATES_PATTERN = re.compile(
        "bbox\s(-?[0-9]+)\s(-?[0-9]+)\s(-?[0-9]+)\s(-?[0-9]+)")

    def __init__(self, hocr_elem_etree, parent, next_tag, next_attribute, next_class):
        self.__coordinates = (0, 0, 0, 0)
        self._hocr_elem = hocr_elem_etree  # type: etree.Element
        self._id = None
        self._parent = parent
        self._elements = self._parse(next_tag, next_attribute, next_class)

    def _parse(self, next_tag, next_attributte, next_class):

        self._id = self._hocr_elem.get('id', None)

        try:
            title = self._hocr_elem.get('title', None)
            if not title:
                raise KeyError()
            match = HOCRElement.COORDINATES_PATTERN.search(title)
            if match:
                self.__coordinates = (int(match.group(1)),
                                      int(match.group(2)),
                                      int(match.group(3)),
                                      int(match.group(4)))
            else:
                raise ValueError("The HOCR element doesn't contain a valid title property")
        except KeyError:
            self.__coordinates = (0, 0, 0, 0)

        elements = []
        if next_tag is not None and next_class is not None:
            for html_element in self._hocr_elem.findall(".//{}[@class='{}']".format(next_tag, next_attributte)):
                elements.append(next_class(self, html_element))

        return elements

    @property
    def coordinates(self):
        return self.__coordinates

    @property
    def xywh(self):
        c = self.coordinates
        return (
            c[0], c[1], c[2] - c[0], c[3] - c[1]
        )

    @property
    def vertices(self):
        xywh = self.xywh
        return [
            (xywh[0], xywh[1]), (xywh[0] + xywh[2], xywh[1]),
            (xywh[0] + xywh[2], xywh[1] + xywh[3]), (xywh[0], xywh[1] + xywh[3])
        ]

    @property
    def html(self):
        return self._hocr_elem.prettify()

    @property
    def id(self):
        return self._id

    @property
    def parent(self):
        return self._parent

    @property
    def children(self):
        return []

    def __hash__(self):
        return hash(self._id)

    def __eq__(self, other):
        if not isinstance(other, HOCRElement):
            return False
        else:
            return self._id == other._id

    @property
    @abstractmethod
    def ocr_text(self):
        pass


class HOCRDocument(HOCRElement):

    def __init__(self, source, is_path=False):

        if not is_path:
            hocr_html = etree.HTML(source)
        else:
            hocr_html = etree.HTML(open(source, 'rb').read().decode('utf-8'))

        super(HOCRDocument, self).__init__(hocr_html, None, 'div', Page.HOCR_PAGE_TAG, Page)

    @property
    def ocr_text(self):
        output = ""
        for element in self._elements[:-1]:
            output += element.ocr_text
            output += "\n\n"
        output += self._elements[-1].ocr_text
        return output

    @property
    def pages(self):
        return self._elements

    @property
    def children(self):
        return self.pages

    @property
    def npages(self):
        return len(self._elements)


class Page(HOCRElement):

    HOCR_PAGE_TAG = "ocr_page"

    def __init__(self, parent, hocr_html):
        super(Page, self).__init__(hocr_html, parent, 'div', Area.HOCR_AREA_TAG, Area)

    @property
    def ocr_text(self):
        output = ""
        for element in self._elements[:-1]:
            output += element.ocr_text
            output += "\n\n"
        output += self._elements[-1].ocr_text
        return output

    @property
    def areas(self):
        return self._elements

    @property
    def children(self):
        return self.areas

    @property
    def nareas(self):
        return len(self._elements)


class Area(HOCRElement):

    HOCR_AREA_TAG = "ocr_carea"

    def __init__(self, parent, hocr_html):
        super(Area, self).__init__(hocr_html, parent, 'p', Paragraph.HOCR_PAR_TAG, Paragraph)

    @property
    def paragraphs(self):
        return self._elements

    @property
    def children(self):
        return self.paragraphs

    @property
    def nparagraphs(self):
        return len(self._elements)

    @property
    def ocr_text(self):
        output = ""
        for element in self._elements[:-1]:
            output += element.ocr_text
            output += "\n"
        output += self._elements[-1].ocr_text
        return output


class Paragraph(HOCRElement):

    HOCR_PAR_TAG = "ocr_par"

    def __init__(self, parent, hocr_html):
        super(Paragraph, self).__init__(hocr_html, parent, 'span', Line.HOCR_LINE_TAG, Line)

    @property
    def lines(self):
        return self._elements

    @property
    def children(self):
        return self.lines

    @property
    def nlines(self):
        return len(self._elements)

    @property
    def ocr_text(self):
        output = ""
        for element in self._elements[:-1]:
            output += element.ocr_text
            output += "\n"
        output += self._elements[-1].ocr_text
        return output


class Line(HOCRElement):

    HOCR_LINE_TAG = "ocr_line"

    def __init__(self, parent, hocr_html):
        super(Line, self).__init__(hocr_html, parent, 'span', Word.HOCR_WORD_TAG, Word)

    @property
    def words(self):
        return self._elements

    @property
    def children(self):
        return self.words

    @property
    def nwords(self):
        return len(self._elements)

    @property
    def ocr_text(self):
        output = ""
        for element in self._elements[:-1]:
            output += element.ocr_text
            output += " "
        output += self._elements[-1].ocr_text
        return output


class Word(HOCRElement):

    HOCR_WORD_TAG = "ocrx_word"

    def __init__(self, parent, hocr_html):
        super(Word, self).__init__(hocr_html, parent, None, None, None)

    @property
    def ocr_text(self):
        word = etree.tounicode(self._hocr_elem[0], method='text', with_tail=False)
        if word is not None:
            return word
        else:
            return ""
